package com.poc.customerservice.service;


import com.poc.customerservice.dto.CustomerRequestDTO;
import com.poc.customerservice.dto.CustomerResponseDTO;

import java.util.List;

public interface ICustomerService {

    List<CustomerResponseDTO> getCustomers();
    CustomerResponseDTO addCustomer(CustomerRequestDTO customerRequestDTO);
    CustomerResponseDTO getCustomerById(Long customerId);
    CustomerResponseDTO updateCustomer(CustomerRequestDTO customerRequestDTO);
    void deleteCustomer(Long id);
}
