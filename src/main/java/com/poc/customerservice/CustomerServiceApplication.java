package com.poc.customerservice;

import com.poc.customerservice.dto.CustomerRequestDTO;
import com.poc.customerservice.entity.Customer;
import com.poc.customerservice.service.CustomerService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CustomerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(CustomerService customerService) {
        return args -> {
            customerService.addCustomer(new CustomerRequestDTO(null, "Youssef", "youssef@gmail.com"));
            customerService.addCustomer(new CustomerRequestDTO(null, "Amine", "amine@gmail.com"));
        };
    }
}
