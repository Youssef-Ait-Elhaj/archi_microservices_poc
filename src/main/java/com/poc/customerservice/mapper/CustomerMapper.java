package com.poc.customerservice.mapper;

import com.poc.customerservice.dto.CustomerRequestDTO;
import com.poc.customerservice.dto.CustomerResponseDTO;
import com.poc.customerservice.entity.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerResponseDTO customerToCustomerResponseDTO(Customer c);
    Customer customerRequestDTOCustomer(CustomerRequestDTO customerRequestDTO);
}
