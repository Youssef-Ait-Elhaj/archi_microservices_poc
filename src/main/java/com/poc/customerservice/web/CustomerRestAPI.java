package com.poc.customerservice.web;


import com.poc.customerservice.dto.CustomerRequestDTO;
import com.poc.customerservice.dto.CustomerResponseDTO;
import com.poc.customerservice.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/customers")
public class CustomerRestAPI {

    private CustomerService customerService;

    public CustomerRestAPI(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(path = "/")
    public List<CustomerResponseDTO> getCustomers() {
        return customerService.getCustomers();
    }

    @PostMapping(path = "/add")
    public CustomerResponseDTO addCustomer(@RequestBody CustomerRequestDTO customerRequestDTO) {
        return customerService.addCustomer(customerRequestDTO);
    }

    @GetMapping(path = "/{id}")
    public CustomerResponseDTO getCustomer(@PathVariable Long id) {
        return customerService.getCustomerById(id);
    }
}
